#!/bin/sh

TIMEOUT=120

ERRCODE=1

set -eu
IP=`echo "$1"|cut -d: -f1`
PORT=`echo "$1"|cut -d: -f2`
set +eu


for i in `seq 1 $TIMEOUT`;do
    nc -n -w 1 -z "$IP" "$PORT" && echo -n "ok" && ERRCODE=0 && break
    echo -n '.'
    sleep 1
done

if [ $ERRCODE -eq 0 ];then
    echo ""
fi

exit $FAILED

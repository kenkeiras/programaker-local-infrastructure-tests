#!/bin/sh

set -eu

cd "$(dirname "$0")"

./setup.sh

echo "Starting tests..."

./run-tests.sh

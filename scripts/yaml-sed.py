#!/usr/bin/env python3

import sys

import yaml

if len(sys.argv) != 3:
    print("{} <selector> <new value>".format(sys.argv[0]))
    exit(1)

data = yaml.load(sys.stdin.read())


print(yaml.dump(data))

#!/bin/sh

set -eu

cd "$(dirname "$0")"

cd ../description/

echo "01 - Time service killed: Should re-start"
./fails/01-time-service-killed-run.sh
./fails/01-time-service-killed-check.sh

echo "02 - Secondary server killed: Should re-merge"
./fails/02-secondary-server-stopped-run.sh
./fails/02-secondary-server-stopped-check.sh

echo "03 - Primary server killed"
./fails/03-primary-server-stopped-run.sh
./fails/03-primary-server-stopped-check.sh

#!/bin/sh

TIMEOUT=120

ERRCODE=1

for i in `seq 1 $TIMEOUT`;do
    curl -m 1 -f -S -k https://minikube/api/v0/ping 1>/dev/null 2>/dev/null  && echo -n "ok" && ERRCODE=0 && break
    echo -n '.'
    sleep 1
done

if [ $ERRCODE -eq 0 ];then
    echo ""
else
    echo
    echo "Could not connect to backend in the expected time"
fi

exit $ERRCODE

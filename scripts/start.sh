#!/bin/sh

set -eu

minikube start --driver=docker --insecure-registry "10.0.0.0/24"

#!/bin/sh

set -eu

SKIP_START=${SKIP_START:-}
export BACKEND_HOST=minikube

if [ -z "$BACKEND_IMAGE" ];then
    echo "Environment variable 'BACKEND_IMAGE' not defined"
fi

cd "$(dirname "$0")"

TOOLS=`pwd`

if [ "$SKIP_START" = "" ];then
    ./start.sh
else
    echo "Skipping start"
fi

minikube addons enable ingress

cd ../description/

# kubectl apply -f namespaces.yaml

# Spawn prometheus & grafana
cd metrics

kubectl apply -f 01-prometheus-config.yaml
kubectl apply -f 02-prometheus-deployment.yaml
kubectl apply -f 03-prometheus-service.yaml

PROMETHEUS_URL=`minikube  service --url metrics-prometheus-ext`

# Spawn Registry
cd ../registry

kubectl apply -f 01-service.yaml
kubectl apply -f 02-registry.yaml

REGISTRY_URL=`minikube service --url registry-ext`
REGISTRY=$(echo "${REGISTRY_URL}"| sed -r 's/^https?:\/\///')

# Check that local registry is allowed as insecure
TO_CONFIG=1
CONFIG_FILE=/etc/docker/daemon.json
if [ -f "$CONFIG_FILE"  ];then
    set +eu
    jq '.["insecure-registries"]' < "$CONFIG_FILE" | grep "$REGISTRY_URL" > /dev/null
    TO_CONFIG=$?
    set -eu
fi

echo "Need to configure Docker daemon for registry? $TO_CONFIG"

if [ $TO_CONFIG -ne 0 ];then
    echo "Configuring docker daemon"
    if [ ! -f "$CONFIG_FILE" ];then
        echo "{ \"insecure-registries\" : [ \"$REGISTRY_URL\" ]}" | sudo tee "$CONFIG_FILE"
    else
        new_value=$(jq <"$CONFIG_FILE" ".[\"insecure-registries\"] |= . + [ \"$REGISTRY_URL\" ]")
        echo "$new_value" | sudo tee "$CONFIG_FILE"
    fi
    echo "Reloading docker"
    sudo systemctl reload docker
fi

# Upload backend image to registry
docker image ls |grep "$BACKEND_IMAGE" >> /dev/null || docker pull "$BACKEND_IMAGE"

BACKEND_VERSION=$(docker image ls $BACKEND_IMAGE|awk '{ print $3; }'|tail -n-1)

set -x
docker tag "$BACKEND_IMAGE" "$REGISTRY/backend-rolling:$BACKEND_VERSION"
"$TOOLS"/wait-for-port.sh "$REGISTRY"

docker push "$REGISTRY/backend-rolling:$BACKEND_VERSION"
set +x

export INTERNAL_REGISTRY=$(kubectl get svc local-registry -o json| jq -r .spec.clusterIP)
export BACKEND_IMAGE="${INTERNAL_REGISTRY}:5000/backend-rolling:$BACKEND_VERSION"

# Spawn elements
cd ../backend

envsubst < 02-ingress.yaml.orig > 02-ingress.yaml
envsubst < 05-statefulset.yaml.orig > 05-statefulset.yaml

sh 00-create-secret.sh
kubectl apply -f 01-service.yaml
kubectl apply -f 02-ingress.yaml
kubectl apply -f 03-sys.config.configmap
kubectl apply -f 04-shared-volume.yaml
kubectl delete pods --grace-period=1 plaza-backend-0 plaza-backend-1 plaza-backend-2 2> /dev/null || true
kubectl apply -f 05-statefulset.yaml

"$TOOLS"/wait-for-backend.sh

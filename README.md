# Programaker local infrastructure tests

This is a collection of scripts to setup a minikube environment, install a PrograMaker backend cluster (with 3 nodes) inside of it and run some resilience tests.

## Dependencies 

- Docker
- Kubectl
- Minikube
- Jq
- ... :TODO:

## How to run

 1. Set the environment variable `BACKEND_IMAGE` to a PrograMaker backend image (like `registry.gitlab.com/programaker-project/programaker-core/backend:4d7b4de1d0c5671fb6d685691ccdf82d026a7921`).
 2. Launch `./scripts/run-e2e.sh`

## Tests run 

These are defined on the `description/fails` folder.

 1. Stop the `automate_services_time` service, and check that it gets up again.
 2. Stop a **secondary** node, and check that it gets re-merged on the cluster again.
 3. Stop the **primary** node, and check that it gets re-merged on the cluster again.

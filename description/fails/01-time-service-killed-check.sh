#!/bin/sh

POD=${POD:-0}

RESULT=`kubectl exec -it plaza-backend-$POD /app/scripts/run_erl.sh -- " \
        T = mnesia:dirty_read(automate_coordination_run_once_tasks, automate_services_time), \
        P = element(4, lists:nth(1, T)), \
        io:fwrite(\"PID: ~p. Node: ~p. Alive: ~p~n\", [P, node(P), automate_coordination_utils:is_process_alive(P)]) \
"`

echo "$RESULT" | grep Alive
echo "$RESULT" | grep 'Alive: true' >/dev/null

exit $?

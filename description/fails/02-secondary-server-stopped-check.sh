#!/bin/sh

set -eu

POD=${POD:-0}

TIMEOUT=120
TARGET_NODE_COUNT=3
IT_COUNTER=1

while [ 1 ];do
    RESULT=`kubectl exec -it plaza-backend-$POD /app/scripts/run_erl.sh -- "length(mnesia:system_info(running_db_nodes))"| grep -v '^ok$'|tr -d '\n\r'`

    echo -n "$RESULT" >&2
    if [ -z "$RESULT" ];then
        echo "Error on primary node"
        exit 2
    fi

    if [ "$RESULT" -ge $TARGET_NODE_COUNT ];then
        echo "(ok)"
        exit 0
    fi
    if [ $IT_COUNTER -ge $TIMEOUT ];then
        echo ""
        echo "Not reached the goal of $TARGET_NODE_COUNT (last count: $RESULT)"
        exit 1
    fi

    IT_COUNTER=$(( $IT_COUNTER + 1 ))

    sleep 1
done

#!/bin/sh

set -eu

SECONDARY_POD=${SECONDARY_POD:-1}

kubectl delete pod --grace-period=1 plaza-backend-$SECONDARY_POD

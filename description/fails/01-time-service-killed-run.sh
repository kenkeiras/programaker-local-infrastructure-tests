#!/bin/sh

set -eu

POD=${POD:-0}

kubectl exec -it plaza-backend-$POD /app/scripts/run_erl.sh -- " \
        T = mnesia:dirty_read(automate_coordination_run_once_tasks, automate_services_time), \
        P = element(4, lists:nth(1, T)), \
        io:fwrite(\"PID: ~p. Node: ~p. Was alive: ~p~n\", [P, node(P), automate_coordination_utils:is_process_alive(P)]), \
        exit(P, fault), \
        io:fwrite(\"Failure injected~n\") \
"

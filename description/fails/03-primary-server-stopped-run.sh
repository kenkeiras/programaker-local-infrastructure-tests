#!/bin/sh

set -eu

PRIMARY_POD=${PRIMARY_POD:-0}

kubectl delete pod --grace-period=1 plaza-backend-$PRIMARY_POD

#!/bin/sh

SECRET_NAME=plaza-backend-erlang-cookie
SECRET_NAMESPACE=default #plaza-production

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 1>/dev/null 2>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

ERLANG_COOKIE=`openssl rand -base64 42|tr -d '='`

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" --from-literal=erlang_cookie="$ERLANG_COOKIE"
